package model;
import java.util.ArrayList;

public class Seats {
	SeatPrice seatPrice;
	double[][] indexSeat;
	ArrayList<String> list;
	ArrayList<String> emptyList;
	
	public Seats(){
		seatPrice = new SeatPrice();
		indexSeat = seatPrice.getDataSeatPrice();
	}
	
	public double getPrice(int i,int j){
		double total = indexSeat[i][j];
		return total;
	}
	
	public void setIndexPrice(int i, int j){
		indexSeat[i][j]=0;
	}
	
	public ArrayList<String> getPriceSelection(double price){
		 list = new ArrayList<String>(); 
		 for (int i = 0; i < 15; i++) {
			for (int j  = 0; j < 20; j++) {
				double cost = indexSeat[i][j];
				if(cost == price){
					list.add(i+" "+j);
				}
			}
		}
		return list;
	}
	
	public ArrayList<String> getEmptySeat(){
		emptyList = new ArrayList<String>();
		for (int i = 0; i < 15; i++) {
			for (int j =0; j < 20; j++) {
				double cost = indexSeat[i][j];
				if(cost <= 0){
					String row = "";
					if(i == 0){
						row = " ";
					}
					else if (i==1){
						row = "B";
					}
					else if (i==2){
						row = "C";
					}
					else if (i==3){
						row = "D";
					}
					else if (i==4){
						row = "E";
					}
					else if (i==5){
						row = "F";
					}
					else if (i==6){
						row = "G";
					}
					else if (i==7){
						row = "H";
					}
					else if (i==8){
						row = "I";
					}
					else if (i==9){
						row = "J";
					}
					else if (i==10){
						row = "K";
					}
					else if (i==11){
						row = "L";
					}
					else if (i==12){
						row = "M";
					}
					else if (i==13){
						row = "N";
					}
					else {
						row = "O";
					}
					list.add(row+j+"");
				}
			}
		}
		return emptyList;	
	}

	public double[][] getSeatAvailable() {
		return indexSeat;
	}

}
