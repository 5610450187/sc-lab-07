

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Times;


public class TheaterSystemGUI implements ActionListener {

	private TheaterManagement tm;

	private JFrame frame;
	private JPanel panelTh;
	private JPanel panelSelec;
	private JPanel panelPrice;
	private JLabel labelSelecSeat;
	private JLabel labelPrice;
	private JLabel labelSeat;
	private JLabel labelTimes;
	private JComboBox comboSelectSeat;
	private JLabel labelBaht;
	private JButton btnSelecSeat;
	private JButton btnBuy;
	private JButton btnEmpty;
	private JButton btnTemp;
	private JButton[][] arrayButton;
	
	private int intRow,intClm;
	
	public TheaterSystemGUI() {
		
		tm = new TheaterManagement();
		arrayButton = new JButton[15][20];
		btnTemp = new JButton();
		intRow = 0;
		intClm = 0;
		
		frame = new JFrame();
		frame.setSize(1200, 700);
		frame.setLayout(new BorderLayout());
		
		panelSelec = new JPanel();
		panelPrice = new JPanel();
				
		labelSelecSeat = new JLabel("Seat Price : ");
		comboSelectSeat = new JComboBox();
		comboSelectSeat.addItem("10");
		comboSelectSeat.addItem("20");
	    comboSelectSeat.addItem("30");
	    comboSelectSeat.addItem("40");
	    comboSelectSeat.addItem("50");
	    labelBaht = new JLabel(" THB");
		btnSelecSeat = new JButton("Select");	
		
		Times time  = new Times();
		System.out.println(time.getTime_now());
		labelTimes = new JLabel("Now Time : "+ time.getTime_now());
		labelPrice = new JLabel("0.0 THB");
		labelSeat = new JLabel("Total Price : ");

		btnBuy = new JButton("BUY");	
		
		btnEmpty = new JButton("SeatAvailable");
		
		panelSelec.setLayout(new GridLayout(1,3));
		btnSelecSeat.addActionListener(this);		
		panelSelec.add(labelSelecSeat);
		panelSelec.add(comboSelectSeat);
		panelSelec.add(labelBaht);
		panelSelec.add(btnSelecSeat);
		panelSelec.add(labelTimes);
		
		
		panelPrice.setLayout(new GridLayout(3, 2));
		btnBuy.addActionListener(this);
		
		btnEmpty.addActionListener(this);
		panelPrice.add(labelSeat);
		panelPrice.add(labelPrice);		
		panelPrice.add(btnBuy);	
		panelPrice.add(btnEmpty);
		panelPrice.setVisible(true);
		

		frame.add(setPanel(tm.getSeat().getSeatAvailable(),"PRICE") , BorderLayout.NORTH);
		JPanel pnlTmp = new JPanel();
		pnlTmp.setLayout(new BorderLayout());
		pnlTmp.add(panelSelec,BorderLayout.NORTH);
		pnlTmp.add(panelPrice,BorderLayout.WEST);
		
		frame.add(pnlTmp,BorderLayout.WEST);
				
		frame.setVisible(true);
		

	
	}
	
	public JPanel setPanel(double arraySeat[][],String str){
		int i,j;
		panelTh = new JPanel();
		panelTh.setLayout(new GridLayout(16,21));
		panelTh.setSize(600,600);
		Icon ani = new ImageIcon("chair.gif");
		for(i=0; i<=15; i++)
		{
			for(j=0; j<=20; j++)
			{				
				if(i == 0)
				{
					if(j != 0)
						panelTh.add(new JLabel(""+j),i,j);
						
					else
						panelTh.add(new JLabel("  "),i,j);
				}
				else if(j==0) 
				{
					panelTh.add(new JLabel(""+ (char)(65+i-1)));
				}
				else
				{
					double price = tm.buyerIndexTicket(i-1, j-1);
					
					if(price <0 ){
						JButton btnTmp;
						btnTmp = new JButton("");
						arrayButton[i-1][j-1] = btnTmp;
						btnTmp.setEnabled(false);
						panelTh.add(btnTmp);
					}
					else {
						JButton btnTmp;
						btnTmp = new JButton(ani);
						String name = String.valueOf(i)+" "+String.valueOf(j);
						btnTmp.setText(name);
						arrayButton[i-1][j-1] = btnTmp;
						btnTmp.addActionListener(this);
						panelTh.add(btnTmp);	
					}

				}
			}
		}		
		panelTh.setVisible(true);
		return  panelTh;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		Object obj = e.getSource();
		
		if(obj instanceof JButton){
			JButton button = (JButton) obj;

			if("BUY".equals(str)){
				btnTemp.setEnabled(false);
				labelPrice.setText("0.0 THB");	
				String name = btnTemp.getText();
				int count=0;
				String i = "";
				String j = "";
				StringTokenizer token = new StringTokenizer(name);
				while(token.hasMoreTokens()){
					if(count==0){
						i = token.nextToken();
						count++;
					}
					else{
						j = token.nextToken();
					}
					
				}
				int row = Integer.parseInt(i);
				int col = Integer.parseInt(j);
				tm.setIndexPrice(row-1,col-1);
				


			}
			else if ("SeatAvailable".equals(str)){
				getSeatAvailable();	
			}
			else if ("Select".equals(str)){			
				double price = Double.parseDouble((String)comboSelectSeat.getSelectedItem());
				ArrayList<String> list = tm.findIndexPrice(price);
				for(int i=0;i<15;i++){
					for(int j=0;j<20;j++){						
						arrayButton[i][j].setEnabled(false);						
					}
				}
			
				for (String index : list) {
					int count=0;
					String i = "";
					String j = "";
					StringTokenizer token = new StringTokenizer(index);
					while(token.hasMoreTokens()){
						if(count==0){
							i = token.nextToken();
							count++;
						}
						else{
							j = token.nextToken();
						}
					}
					int row = Integer.parseInt(i);
					int col = Integer.parseInt(j);
					arrayButton[row][col].setEnabled(true);
					
				}
			}
			else{
				btnTemp = button;
				String name = btnTemp.getText();
				int count=0;
				String i = "";
				String j = "";
				StringTokenizer token = new StringTokenizer(name);
				while(token.hasMoreTokens()){
					if(count==0){
						i = token.nextToken();
						count++;
					}
					else{
						j = token.nextToken();
					}
					
				}
				
				labelPrice.setText(tm.buyerIndexTicket(Integer.parseInt(i)-1, Integer.parseInt(j)-1)+" THB");

				
			}
			
		}
	}

	private void getSeatAvailable() {
			int i,j;				
			double[][] arraySeatAvailable = tm.getSeatAvailable().clone();
			
			for(i=0; i<15; i++)
			{
				for(j=0; j<20 ; j++)
				{
					JButton btnTmp = arrayButton[i][j];
					if(btnTmp!= null)
					{
						if(arraySeatAvailable[i][j]>0)
						{								
							btnTmp.setEnabled(true);
						}
						else 
						{
							btnTmp.setEnabled(false);
						}
					}
				}
			}
		}
		
}
