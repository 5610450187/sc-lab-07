
import java.util.ArrayList;

import model.Seats;
import model.Times;


public class TheaterManagement {
	Seats seats;
	Times time;
	double [][] arraySeatAvailable;
	ArrayList<String> list;
	
	public TheaterManagement(){
		seats = new Seats();
		arraySeatAvailable = seats.getSeatAvailable();
	}
	
	public double buyerIndexTicket(int i,int j){
		double price = seats.getPrice(i,j);
		return price;
	}
	
	public void setIndexPrice(int i,int j) {
		seats.setIndexPrice(i,j);
	}
	
	public ArrayList<String> findIndexPrice(double price){
		list = seats.getPriceSelection(price);
		return list;
	}
	
	public Seats getSeat(){
		return seats;
	}
	
	public double[][] getSeatAvailable(){
		return arraySeatAvailable;
	}
	
	public void EmptySeat(){
		seats.getEmptySeat();
		
	}
	public  String getTimes() {
		return time.getTime_now();
	}
}
